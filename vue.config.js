module.exports = {
  transpileDependencies: ["vuetify"],
  productionSourceMap: false,
  pluginOptions: {
    i18n: {
      enableInSFC: true
    }
  }
};
