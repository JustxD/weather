import Vue from "vue";
import Vuetify from "vuetify/lib/framework";
import ru from "vuetify/lib/locale/ru";
import en from "vuetify/lib/locale/en";

Vue.use(Vuetify);

const theme = JSON.parse(localStorage.getItem("dark")) || false;

export default new Vuetify({
  lang: {
    locales: { ru, en },
    current: "ru"
  },
  theme: {
    dark: theme !== false
  }
});
