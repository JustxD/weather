import Vue from "vue";
import App from "@/App.vue";
import vuetify from "@/plugins/vuetify";
import { store } from "@/store";
import i18n from "@/plugins/i18n";

import "roboto-fontface/css/roboto/roboto-fontface.css";
import "@mdi/font/css/materialdesignicons.css";

Vue.config.productionTip = false;

new Vue({
  vuetify,
  i18n,
  store,
  render: h => h(App)
}).$mount("#app");
