import * as types from "../mutation_types";
import { getWeatherByCityName } from "@/services/api/weather";

const getters = {
  cities: state => state.cities,
  citiesWeather: state => state.citiesWeather
};

const actions = {
  getWeather({ commit }, { city, locale }) {
    return new Promise((resolve, reject) => {
      getWeatherByCityName(city, locale)
        .then(({ data, status }) => {
          if (status === 200) {
            data.name = city;
            commit(types.REQUEST_CITY_WEATHER_SUCCESS, data);
            return resolve();
          } else {
            throw Error(status);
          }
        })
        .catch(error => {
          return reject(error.message);
        });
    });
  },
  deleteCity({ commit }, city) {
    commit(types.DELETE_CITY, city);
  },
  deleteCityWeather({ commit }, id) {
    commit(types.DELETE_CITY_WEATHER, id);
  }
};

const mutations = {
  [types.REQUEST_CITY_WEATHER_SUCCESS](state, data) {
    const cityIndex = state.cities.indexOf(data.name);
    if (cityIndex === -1) {
      state.cities.unshift(data.name);
    }
    const cityWeatherIndex = state.citiesWeather.findIndex(
      v => v.id === data.id
    );
    data.date = new Date();
    if (cityWeatherIndex === -1) {
      state.citiesWeather.push(data);
    } else {
      state.citiesWeather[cityWeatherIndex] = data;
      state.citiesWeather = [...state.citiesWeather];
    }
  },
  [types.DELETE_CITY](state, data) {
    const cityIndex = state.cities.indexOf(data);
    if (cityIndex !== -1) {
      state.cities.splice(cityIndex, 1);
    }
  },
  [types.DELETE_CITY_WEATHER](state, id) {
    const cityWeatherIndex = state.citiesWeather.findIndex(v => v.id === id);
    if (cityWeatherIndex !== -1) {
      state.citiesWeather.splice(cityWeatherIndex, 1);
    }
  }
};

const state = {
  cities: JSON.parse(localStorage.getItem("cities")) || [],
  citiesWeather: []
};

export default {
  state,
  getters,
  actions,
  mutations
};
