const baseURL =
  process.env.VUE_APP_API_URL || "https://api.openweathermap.org/data/2.5/";

const API_KEY = process.env.VUE_APP_API_KEY || "";

const urlCreator = (uri, params) => {
  var url = new URL(baseURL);
  url.pathname += uri;
  url.search = new URLSearchParams({ ...params, appid: API_KEY }).toString();
  return url;
};

export const getWeatherByCityName = (q, lang) => {
  const url = urlCreator("weather", { q, lang });
  return fetch(url)
    .then(async r => {
      return { status: r.status, data: await r.json() };
    })
    .then(data => Promise.resolve(data))
    .catch(error => Promise.reject(error));
};
